package com.iutdm.beerapp;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.iutdm.beerapp.model.Beer;
import com.squareup.picasso.Picasso;

public class BeerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beer_details);
        Beer beer = getIntent().getParcelableExtra("beer");
        if(beer==null) {
            finish();
            return;
        }

        TextView beerName = findViewById(R.id.beerName);
        beerName.setText(beer.getName());

        TextView beerDetails = findViewById(R.id.beerDetails);
        beerDetails.setText(beer.getDetails());

        ImageView beerImage = findViewById(R.id.beerImage);
        Picasso.get()
                .load(beer.getImageURL())
                .error(R.drawable.ic_launcher_foreground)
                .into(beerImage);

        ImageView beerColor = findViewById(R.id.beerColor);
        Picasso.get()
                .load("https://singlecolorimage.com/get/" + beer.getColor() + "/100x25")
                .into(beerColor);

        TextView beerDate = findViewById(R.id.beerDate);
        beerDate.setText(beer.getDate());

        ListView beerFoodPairing = findViewById(R.id.foodPairing);
        ArrayAdapter<String> foodPairingArrayAdapter = new ArrayAdapter<>(this, R.layout.simple_string_list, R.id.value, beer.getFoodPairing().toArray(new String[0]));
        beerFoodPairing.setAdapter(foodPairingArrayAdapter);
        foodPairingArrayAdapter.notifyDataSetChanged();

        ListView beerHops = findViewById(R.id.beerHops);
        ArrayAdapter<String> hopsArrayAdapter = new ArrayAdapter<>(this, R.layout.simple_string_list, R.id.value, beer.getHops().toArray(new String[0]));
        beerHops.setAdapter(hopsArrayAdapter);
        hopsArrayAdapter.notifyDataSetChanged();

        ListView beerMalt = findViewById(R.id.beerMalt);
        ArrayAdapter<String> maltArrayAdapter = new ArrayAdapter<>(this, R.layout.simple_string_list, R.id.value, beer.getMalt().toArray(new String[0]));
        beerMalt.setAdapter(maltArrayAdapter);
        maltArrayAdapter.notifyDataSetChanged();

        TextView tips = findViewById(R.id.BeerTips);
        tips.setText(beer.getTips());

    }
}
