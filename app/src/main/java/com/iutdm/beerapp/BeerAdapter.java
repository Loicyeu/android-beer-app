package com.iutdm.beerapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iutdm.beerapp.model.Beer;
import com.squareup.picasso.Picasso;

import java.util.List;



public class BeerAdapter extends ArrayAdapter<Beer> {
    LayoutInflater inflater;

    public BeerAdapter(Activity context, List<Beer> items) {
        super(context, 0, items);
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder vh;

        if (row == null) {
            row = inflater.inflate(R.layout.beer_list, parent, false);
            vh = new ViewHolder();
            vh.beerImage = row.findViewById(R.id.beerImage);
            vh.beerName = row.findViewById(R.id.beerName);
            vh.beerSlogan = row.findViewById(R.id.beerSlogan);
            row.setTag(vh);
        } else {
            vh = (ViewHolder) row.getTag();
        }

        Beer beer = getItem(position);

        ImageView beerImage = vh.beerImage;
        Picasso.get()
                .load(beer.getImageURL())
                .error(R.drawable.ic_launcher_background)
                .into(beerImage);

        TextView beerName = vh.beerName;
        beerName.setText(beer.getName());

        TextView beerSlogan = vh.beerSlogan;
        beerSlogan.setText(beer.getSlogan());

        return row;
    }

    static class ViewHolder {
        ImageView beerImage;
        TextView beerName;
        TextView beerSlogan;
    }

}