package com.iutdm.beerapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.iutdm.beerapp.model.api.Filter;
import com.iutdm.beerapp.model.api.PunkApi;

public class FilterDialog extends AppCompatDialogFragment {

    private FilterDialogListener listener;
    private SeekBar seekBar;
    private TextView beerNumber;
    private TextView ids, ebcMin, ebcMax, brewedAfter, brewedBefore, yeast, food;
    private final PunkApi punkApi;

    public FilterDialog(PunkApi punkApi) {
        this.punkApi = punkApi;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.filter_dialog, null);

        seekBar = view.findViewById(R.id.seekBar);
        beerNumber = view.findViewById(R.id.beerNumber);
        ids = view.findViewById(R.id.beerIds);
        ebcMin = view.findViewById(R.id.EBCmin);
        ebcMax = view.findViewById(R.id.EBCmax);
        brewedAfter = view.findViewById(R.id.brewedAfter);
        brewedBefore = view.findViewById(R.id.brewedBefore);
        yeast = view.findViewById(R.id.yeast);
        food = view.findViewById(R.id.food);

        if (punkApi.getFilter(Filter.EBC_GT) != null) {
            ebcMin.setText(punkApi.getFilter(Filter.EBC_GT));
        }
        if (punkApi.getFilter(Filter.EBC_LT) != null) {
            ebcMax.setText(punkApi.getFilter(Filter.EBC_LT));
        }
        if (punkApi.getFilter(Filter.IDS) != null) {
            ids.setText(punkApi.getFilter(Filter.IDS));
        }
        if (punkApi.getFilter(Filter.BREWED_AFTER) != null) {
            brewedAfter.setText(punkApi.getFilter(Filter.BREWED_AFTER));
        }
        if (punkApi.getFilter(Filter.BREWED_BEFORE) != null) {
            brewedBefore.setText(punkApi.getFilter(Filter.BREWED_BEFORE));
        }
        if (punkApi.getFilter(Filter.YEAST) != null) {
            yeast.setText(punkApi.getFilter(Filter.YEAST));
        }
        if (punkApi.getFilter(Filter.FOOD) != null) {
            food.setText(punkApi.getFilter(Filter.FOOD));
        }
        int beerNumberParsed = Integer.parseInt(punkApi.getFilter(Filter.PER_PAGE));

        seekBar.setProgress(beerNumberParsed);
        this.beerNumber.setText(String.valueOf(beerNumberParsed));

        builder.setView(view)
                .setTitle("Filtres")
                .setNegativeButton("Annuler", (dialog, which) -> {/* do nothing*/})
                .setPositiveButton("Appliquer", (dialog, which) -> {
                    PunkApi punkApi = PunkApi.createRequest(view.getContext());
                    if (!(ebcMin.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.EBC_GT, ebcMin.getText().toString()
                                .replace(' ', '_'));
                    }
                    if (!(ebcMax.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.EBC_LT, ebcMax.getText().toString()
                                .replace(' ', '_'));
                    }
                    if (!(ids.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.IDS, ids.getText().toString()
                                .replace(',', '|'));
                    }
                    if (!(brewedAfter.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.BREWED_AFTER, brewedAfter.getText().toString());
                    }
                    if (!(brewedBefore.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.BREWED_BEFORE, brewedBefore.getText().toString());
                    }
                    if (!(yeast.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.YEAST, yeast.getText().toString()
                                .replace(' ', '_'));
                    }
                    if (!(food.getText().toString().length() == 0)) {
                        punkApi.addFilter(Filter.FOOD, food.getText().toString()
                                .replace(' ', '_'));
                    }

                    punkApi.addFilter(Filter.PER_PAGE, Integer.toString(seekBar.getProgress()));

                    listener.applyFilters(punkApi);
                });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                beerNumber.setText(Integer.toString(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (FilterDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implements FilterDialogListener interface");
        }
    }

    public interface FilterDialogListener {
        void applyFilters(PunkApi punkApi);
    }

}
