package com.iutdm.beerapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.iutdm.beerapp.model.Beer;
import com.iutdm.beerapp.model.MainActivityViewModel;
import com.iutdm.beerapp.model.api.Filter;
import com.iutdm.beerapp.model.api.PunkApi;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FilterDialog.FilterDialogListener {

    private static final String BASE_URL = "https://api.punkapi.com/v2/beers";

    private EditText beerInput;
    private Button search, previous, next, filtres;
    private ListView beerListView;

    private Context context;
    private MainActivityViewModel model;

    private List<Beer> beerList;
    private ArrayAdapter<Beer> arrayAdapter;
    private PunkApi punkApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();

        model = new ViewModelProvider(this).get(MainActivityViewModel.class);
        if(model.getBeerList()==null) {
            model.setBeerList(new ArrayList<>());
        }
        beerList = model.getBeerList();

        if(model.getPunkApi()==null) {
            model.setPunkApi(PunkApi.createRequest(context));
        }
        punkApi = model.getPunkApi();

        //region VARIABLES
        beerInput = findViewById(R.id.beerInput);
        search = findViewById(R.id.research);
        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);
        beerListView = findViewById(R.id.beerList);
        filtres = findViewById(R.id.filtre);

        arrayAdapter = new BeerAdapter(this, beerList);
        beerListView.setAdapter(arrayAdapter);
        //endregion

        this.previous.setEnabled(false);
        this.next.setEnabled(false);

        search.setOnClickListener((view) -> {
            String query = beerInput.getText().toString();
            query = query.replaceAll(" ", "_");
            if (query.length() > 0) {
                punkApi.addFilter(Filter.BEER_NAME, query);
            }

            this.punkApi.callAPI((e, beers) -> {
                if (e != null) {
                    getRandomBeer("Aucune bière n'a été trouvé... En voici une aléatoire !");
                } else {
                    updateBeerList(beers, this.punkApi.getPageNumber() > 1);
                }
            });
        });

        previous.setOnClickListener(v -> this.punkApi.previousPage().callAPI((e, beers) -> {
            if (e != null) {
                Toast.makeText(context, "Une erreur inatendue s'est produite...", Toast.LENGTH_SHORT).show();
            } else {
                updateBeerList(beers, this.punkApi.getPageNumber() > 1);
            }
        }));

        next.setOnClickListener(v -> this.punkApi.nextPage().callAPI((e, beers) -> {
            if (e != null) {
                Toast.makeText(context, "Une erreur inatendue s'est produite...", Toast.LENGTH_SHORT).show();
            } else {
                if (beers.size() == 0) {
                    next.setEnabled(false);
                    this.punkApi.previousPage();
                    Toast.makeText(context, "Il n'a pas de page suivante", Toast.LENGTH_LONG).show();
                } else {
                    updateBeerList(beers, this.punkApi.getPageNumber() > 1);
                }
            }
        }));

        beerListView.setOnItemClickListener((parent, view, position, id) -> {
            Intent i = new Intent(MainActivity.this, BeerActivity.class);
            i.putExtra("beer", (Parcelable) parent.getItemAtPosition(position));
            startActivity(i);
        });

        filtres.setOnClickListener(v -> {
            FilterDialog filterDialog = new FilterDialog(punkApi);
            filterDialog.show(getSupportFragmentManager(), "filter dialog");
        });

    }

    private void updateBeerList(List<Beer> beers, boolean enablePrevious) {
        beerList.clear();
        beerList.addAll(beers);
        previous.setEnabled(enablePrevious);
        next.setEnabled(true);
        arrayAdapter.notifyDataSetChanged();
    }

    /**
     * Méthode permettant de demander a l'API une bière aléatoire.
     */
    private void getRandomBeer(final String message) {
        String url = BASE_URL + "/random";
        PunkApi.getRandom(context, (e, beers) -> {
            if (e != null) {
                Toast.makeText(context, "Une erreur inatendue s'est produite...", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                beerList.clear();
                beerList.addAll(beers);
                arrayAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void applyFilters(PunkApi newPunkApi) {
        this.model.setPunkApi(newPunkApi);
        this.punkApi = model.getPunkApi();
    }
}