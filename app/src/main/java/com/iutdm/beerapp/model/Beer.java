package com.iutdm.beerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.iutdm.beerapp.R;

import java.util.ArrayList;
import java.util.List;

public class Beer implements Parcelable {

    public static final Creator<Beer> CREATOR = new Creator<Beer>() {
        @Override
        public Beer createFromParcel(Parcel in) {
            return new Beer(in);
        }

        @Override
        public Beer[] newArray(int size) {
            return new Beer[size];
        }
    };
    private final int id;
    private final String name;
    private final String details;
    private final String slogan;
    private final String imageURL;
    private final String color; //hex value
    private final String date;
    private String tips;
    private List<String> malt;
    private List<String> hops;
    private List<String> foodPairing;

    protected Beer(Parcel in) {
        id = in.readInt();
        name = in.readString();
        details = in.readString();
        slogan = in.readString();
        imageURL = in.readString();
        color = in.readString();
        date = in.readString();
        tips = in.readString();
        malt = in.createStringArrayList();
        hops = in.createStringArrayList();
        foodPairing = in.createStringArrayList();
    }

    public Beer(JsonObject beer) {
        this.id = beer.get("id").getAsInt();
        this.name = beer.get("name").getAsString();
        this.details = beer.get("description").getAsString();
        this.slogan = beer.get("tagline").getAsString();
        this.date = beer.get("first_brewed").getAsString();

        if (beer.has("image_url") && !beer.get("image_url").isJsonNull()) {
            this.imageURL = beer.get("image_url").getAsString();
        } else {
            this.imageURL = null;
        }

        if (beer.has("ebc") && !beer.get("ebc").isJsonNull()) {
            this.color = ebcToHex(beer.get("ebc").getAsNumber().floatValue());
        } else {
            this.color = null;
        }

        if (beer.has("brewers_tips") && !beer.get("brewers_tips").isJsonNull()) {
            this.tips = beer.get("brewers_tips").getAsString();
        }

        if (beer.has("food_pairing") && !beer.get("food_pairing").isJsonNull()) {
            this.foodPairing = new ArrayList<>();
            for (JsonElement jsonElement : beer.get("food_pairing").getAsJsonArray()) {
                this.foodPairing.add(jsonElement.getAsString());
            }
        }

        if (beer.has("ingredients") && !beer.get("ingredients").isJsonNull()) {
            JsonObject ingredients = beer.get("ingredients").getAsJsonObject();
            if (ingredients.has("malt") && !ingredients.get("malt").isJsonNull()) {
                this.malt = new ArrayList<>();
                for (JsonElement jsonElement : ingredients.get("malt").getAsJsonArray()) {
                    this.malt.add(jsonElement.getAsJsonObject().get("name").getAsString());
                }
            }
            if (ingredients.has("hops") && !ingredients.get("hops").isJsonNull()) {
                this.hops = new ArrayList<>();
                for (JsonElement jsonElement : ingredients.get("hops").getAsJsonArray()) {
                    this.hops.add(jsonElement.getAsJsonObject().get("name").getAsString());
                }
            }
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDetails() {
        return details;
    }

    public String getSlogan() {
        return slogan;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getColor() {
        return color;
    }

    public String getDate() {
        return date;
    }

    public String getTips() {
        return tips;
    }

    public List<String> getMalt() {
        return malt;
    }

    public List<String> getHops() {
        return hops;
    }

    public List<String> getFoodPairing() {
        return foodPairing;
    }

    private String ebcToHex(float ebc) {
        return Ebc2Hex.convert(ebc, 0.5f);
    }


    @Override
    public String toString() {
        return "Beer {"
                + "\n\tid: " + this.id
                + "\n\tnname: " + this.name
                + "\n\tdetails: " + this.details
                + "\n\tslogan: " + this.slogan
                + "\n\timageURL: " + this.imageURL
                + "\n\tcolor: " + this.color
                + "\n\tdate: " + this.date
                + "\n\ttips: " + this.tips
                + "\n\tmalt: " + this.malt
                + "\n\thops: " + this.hops
                + "\n\tfoodPairing: " + this.foodPairing
                + "\n}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(details);
        dest.writeString(slogan);
        dest.writeString(imageURL);
        dest.writeString(color);
        dest.writeString(date);
        dest.writeString(tips);
        dest.writeStringList(malt);
        dest.writeStringList(hops);
        dest.writeStringList(foodPairing);
    }
}

/*
    private int id;
    private String name;
    private String details;
    private String slogan;
    private String imageURL;
    private String color; //hex value
    private String date;
    private String tips;
    private List<String> malt;
    private List<String> hops;
    private List<String> foodPairing;
 */