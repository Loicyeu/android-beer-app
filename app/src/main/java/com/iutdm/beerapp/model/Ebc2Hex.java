package com.iutdm.beerapp.model;

public class Ebc2Hex {
    public static String convert(float ebc, float sat) {
        ebc = ebc > 80 ? 80 : ebc;
        ebc = ebc < 0 ? 0 : ebc;
        float srm = ebcToSrm(ebc);
        RGB rgb = new RGB(
                calcRed(srm),
                calcGreen(srm),
                calcBlue(srm)
        );
        rgb = desaturate(rgb, sat);
        return rgbToHex(rgb);
    }
    public static float ebcToSrm( float ebc) {
        return ebc * 0.508f;
    }
    public static int calcRed(float srm) {
        int result = (int) Math.round(280 - srm * 5.65);
        return Math.min(result, 255);
    }
    public static int calcGreen(float srm) {
        return (int) Math.round(0.188349 * Math.pow(srm, 2) - 13.2676 * srm + 239.51);
    }
    public static int calcBlue(float srm) {
        int result = (int) Math.round(0.000933566 * Math.pow(srm, 4) - 0.0894788 * Math.pow(srm, 3) + 3.00611 * Math.pow(srm, 2) - 40.8883 * srm + 183.409);
        return Math.max(result, 0);
    }
    public static String segmentToHex(int seg) {
        String hex = Integer.toString(seg, 16);
        return hex.length() == 1 ? "0" + hex : hex;
    }
    public static String rgbToHex(RGB rgb) {
        return segmentToHex(rgb.r) + segmentToHex(rgb.g) + segmentToHex(rgb.b);
    }
    public static RGB desaturate(RGB rgb, float sat) {
        float gray = rgb.r * 0.3086f + rgb.g * 0.6094f + rgb.b * 0.0820f;
        gray = gray * (1 - sat);
        rgb.r = Math.round(rgb.r * sat + gray);
        rgb.g = Math.round(rgb.g * sat + gray);
        rgb.b = Math.round(rgb.b * sat + gray);
        return rgb;
    }

    private static class RGB {
        private int r,g,b;

        public RGB(int r, int g, int b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public int getR() {
            return r;
        }

        public void setR(int r) {
            this.r = r;
        }

        public int getG() {
            return g;
        }

        public void setG(int g) {
            this.g = g;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }
    }
}
