package com.iutdm.beerapp.model;

import androidx.lifecycle.ViewModel;

import com.iutdm.beerapp.model.api.PunkApi;

import java.util.List;

public class MainActivityViewModel extends ViewModel {

    private List<Beer> beerList;
    private PunkApi punkApi;

    public void setBeerList(List<Beer> beerList) {
        this.beerList = beerList;
    }

    public void setPunkApi(PunkApi punkApi) {
        this.punkApi = punkApi;
    }

    public List<Beer> getBeerList() {
        return beerList;
    }

    public PunkApi getPunkApi() {
        return punkApi;
    }
}
