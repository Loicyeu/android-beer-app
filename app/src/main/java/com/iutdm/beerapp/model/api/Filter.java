package com.iutdm.beerapp.model.api;

public enum Filter {
    ABV_GT("abv_gt"),
    ABV_IT("abv_it"),
    IBU_GT("ibu_gt"),
    IBU_IT("ibu_it"),
    EBC_GT("ebc_gt"),
    EBC_LT("ebc_lt"),
    BEER_NAME("beer_name"),
    YEAST("yeast"),
    BREWED_BEFORE("brewed_before"),
    BREWED_AFTER("brewed_after"),
    HOPS("hops"),
    MALT("malt"),
    FOOD("food"),
    IDS("ids"),
    PER_PAGE("per_page"),
    PAGE("page");

    private final String filter;

    Filter(String filter) {
        this.filter = filter;
    }

    String getFilterName() {
        return this.filter;
    }
}
