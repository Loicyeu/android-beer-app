package com.iutdm.beerapp.model.api;

import android.content.Context;

import com.google.gson.JsonElement;
import com.iutdm.beerapp.model.Beer;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PunkApi {

    private static final String BASE_URL = "https://api.punkapi.com/v2/beers";

    private final Context context;
    private final Map<Filter, String> filters;

    /**
     * Méthode statique permettant de créer une instance de PunkApi.
     * Par défaut la page donné est la première avec 10 résultats.
     *
     * @param context Le context de l'application.
     * @return La nouvelle instance de PunkApi.
     */
    public static PunkApi createRequest(Context context) {
        return new PunkApi(context);
    }

    /**
     * Constructeur de la classe PunkApi.
     * La page donné par défaut est la première avec 10 résultats
     *
     * @param context Le contexte de l'application.
     */
    private PunkApi(Context context) {
        this.context = context;
        this.filters = new HashMap<>();
        filters.put(Filter.PAGE, "1");
        filters.put(Filter.PER_PAGE, "10");
    }

    /**
     * Méthode permettant d'ajouter un filtre la requête.
     *
     * @param filter Le filtre à ajouter ou modifier.
     * @param value  La valeur associé au filtre.
     * @return L'instance de PunkApi.
     */
    public PunkApi addFilter(Filter filter, String value) {
        this.filters.put(filter, value.trim().replaceAll(" ", "_"));
        return this;
    }

    /**
     * Méthode changeant la page souhaiter par la suivante.
     *
     * @return L'instance de PunkApi
     */
    public PunkApi nextPage() {
        int page;
        try {
            page = Integer.parseInt(this.filters.get(Filter.PAGE)) + 1;
        } catch (NumberFormatException e) {
            page = 1;
        }
        this.filters.put(Filter.PAGE, Integer.toString(page));
        return this;
    }

    /**
     * Méthode changeant la page souhaiter par la précédente.
     *
     * @return L'instance de PunkApi
     */
    public PunkApi previousPage() {
        int page;
        try {
            page = Integer.parseInt(this.filters.get(Filter.PAGE));
            page = page <= 1 ? 1 : page - 1;
        } catch (NumberFormatException e) {
            page = 1;
        }
        this.filters.put(Filter.PAGE, Integer.toString(page));
        return this;
    }

    /**
     * Méthode permettant de faire l'appel à l'API.
     *
     * @param callBack Le callback.
     */
    public void callAPI(PunkApiCallback callBack) {
        Ion.with(context)
                .load(buildUrl())
                .asJsonArray()
                .setCallback((e, result) -> {
                    if (e != null) {
                        callBack.callback(e, null);
                    } else {
                        List<Beer> beerList = new ArrayList<>();
                        for (JsonElement element : result) {
                            beerList.add(new Beer(element.getAsJsonObject()));
                        }
                        callBack.callback(null, beerList);
                    }
                });
    }

    /**
     * Méthode permettant de connaître le numéro de la page.
     *
     * @return Le numéro de la page ou 1 en cas d'erreur.
     */
    public int getPageNumber() {
        try {
            return Integer.parseInt(this.filters.get(Filter.PAGE));
        } catch (NumberFormatException e) {
            return 1;
        }
    }

    /**
     * Méthode permettant de récupérer la valeur d'un filtre utilisé.
     *
     * @param filter Le filtre que l'on souhaite récupérer.
     * @return La valeur associé au filtre ou nul s'il n'y en a pas.
     */
    public String getFilter(Filter filter) {
        return filters.get(filter);
    }

    private String buildUrl() {
        StringBuilder url = new StringBuilder(BASE_URL);
        url.append('?');
        filters.forEach((key, value) -> url.append('&').append(key.getFilterName())
                .append('=').append(value));
        System.out.println(url);
        return url.toString();
    }

    public static void getRandom(Context context, PunkApiCallback callBack) {
        String url = BASE_URL + "/random";
        Ion.with(context)
                .load(url)
                .asJsonArray()
                .setCallback((e, result) -> {
                    if (e != null) {
                        callBack.callback(e, null);
                    } else {
                        List<Beer> beerList = new ArrayList<>();
                        for (JsonElement element : result) {
                            beerList.add(new Beer(element.getAsJsonObject()));
                        }
                        callBack.callback(null, beerList);
                    }
                });
    }

}
