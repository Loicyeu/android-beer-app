package com.iutdm.beerapp.model.api;

import com.iutdm.beerapp.model.Beer;

import java.util.List;

/**
 * Callback de la classe PunkAPI
 * @see PunkApi
 */
public interface PunkApiCallback {

    /**
     * Callback qui prend une liste de bière ou une exception.
     * @param e Une exception ou null s'il n'y en a pas.
     * @param beers La liste de bière ou null s'il y a une erreur.
     */
    void callback(Exception e, List<Beer> beers);
}
